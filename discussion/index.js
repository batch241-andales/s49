// Mock Database
// let posts = [];

// // post ID
// let count = 1;

fetch('https://jsonplaceholder.typicode.com/posts').then((response)=> response.json()).then((data)=> showPosts(data))

// Reactive DOM with JSON (CRUD Operation)


// ADD POST DATA

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// Prevents the default behavior of an event
	// To prevent the page from reloading (default of behavior of submit)
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data)=> {
		console.log(data);
		alert("Post Successfully Added!");
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})

});

// RETRIEVE POST
const showPosts = (posts) => {
	let postEntries = "";
	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onClick="editPost('${post.id}')">Edit</button>
		<button onClick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries
}



// EDIT POST (Edit Button)
const editPost = (id) => {

	// The function first uses the querySelector() method to get the element with the id "#post-title-${id}"
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body;
	document.querySelector("#btn-submit-update").removeAttribute('disabled')
}


// Update POST (update Button)
document.querySelector("#form-edit-post").addEventListener('submit', (e) =>{
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1 
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data)=> {
		console.log(data);
		alert('Post Successfully Updated!')
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector("#btn-submit-update").setAttribute('disabled', true)
	})
})


//delete Post
const deletePost = (id) => {			
			// posts.splice(i,1)
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'
	})
	
	const element = document.querySelector(`#post-${id}`);
	element.remove();
	


	
	// alert("Post Successfully Deleted")

}


